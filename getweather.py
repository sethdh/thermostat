#!/usr/bin/python3

import urllib.request
import json
import os
import time
import datetime

## 2019-jan-01 must be added to prevent kivy from seg faulting
##  github.com/kivy/kivy/issues/6007
os.environ['KIVY_GL_BACKEND'] = 'gl'

##############################################################################
#                                                                            #
#       Kivy UI Imports                                                      #
#                                                                            #
##############################################################################

import kivy
kivy.require( '1.9.0' ) # replace with your current kivy version !

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.slider import Slider
from kivy.clock import Clock
from kivy.graphics import Color, Rectangle
from kivy.storage.jsonstore import JsonStore
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition


##############################################################################
#                                                                            #
#       Weather functions/constants/widgets                                  #
#                                                                            #
##############################################################################

tempScale                = "imperial"
scaleUnits               = "c" if tempScale == "metric" else "f"
windFactor               = 3.6 if tempScale == "metric" else 1.0
windUnits                = " km/h" if tempScale == "metric" else " mph"
weatherLocation          = "41.72,-111.87" ## 41.72,-111.87 is logan
weatherAppKey            = "db151a4c87cf35fea3fa02599bf11b81"
weatherURLBase           = "https://api.darksky.net/forecast"
#weatherURLCurrent        = weatherURLBase + "weather?units=" + tempScale + "&q=" + weatherLocation + "&APPID=" + weatherAppKey
weatherReq                  = weatherURLBase + "/" + weatherAppKey + "/" + weatherLocation
weatherResponse =""
# forecast/daily is only available for paid accounts so get 9 of the 3hr forecasts
#weatherURLForecast       = weatherURLBase + "forecast?cnt=9&units=" + tempScale + "&q=" + weatherLocation + "&APPID=" + weatherAppKey
weatherURLTimeout        = 500000
weatherSummaryLabel  = Label( text="", size_hint = ( None, None ), font_size='20sp', markup=True, text_size=( 200, 20 ) )
weatherDetailsLabel  = Label( text="", size_hint = ( None, None ), font_size='20sp', markup=True, text_size=( 300, 150 ), valign="top" )
weatherImg           = Image( source="web/images/na.png", size_hint = ( None, None ) )


def get_weather( url ):
        print("get_weather in url: {}".format(url))
        jsonobj = ""
        try:
            # gotstuff = urllib.request.urlopen( url ).read().decode('utf-8')
            # gotstuff = urllib.request.urlopen( url, None, weatherURLTimeout ).read()
            gottext = urllib.request.urlopen( url ).read().decode('utf-8')
            print("get_weather stuff: {}".format(gottext))
            # jsonobj = json.dumps(gotstuff)
            jsonobj = json.loads(gottext)
        except Exception as e:
            print("get_weather err: {}".format(e))
        return jsonobj
        # return json.loads(urllib.request.urlopen( url, None, weatherURLTimeout ).read())


def get_cardinal_direction( heading ):
        directions = [ "N", "NE", "E", "SE", "S", "SW", "W", "NW", "N" ]
        return directions[ int( round( ( ( heading % 360 ) / 45 ) ) ) ]

def getTempInUnits( tempF ):
    if (tempScale == "imperial"):
        return tempF
    else:
        return (tempF - 32)*(5/9)


try:
    print("before calling get_weather")
    weatherResponse = get_weather( weatherReq )
    print("after calling get_weather")

    curr = weatherResponse[ "currently" ]
    #weatherImg.source = "web/images/" + curr[ "icon" ] + ".png"
    print("weatherImg.source = {}".format(curr[ "icon" ]))
    weatherSummaryLabel.text = "[b]" + curr[ "summary" ] + "[/b]"
    print("current summary: {}".format(curr[ "summary" ]))
    currTemp = getTempInUnits(curr["temperature"])
    currHumid = curr["humidity"]
    print("temp: {}{} humidity: {}%".format(currTemp,scaleUnits,currHumid))

    #weatherDetailsLabel.text = "\n".join( (
        #"Temp:       " + str( int( round( weatherResponse[ "currently" ][ "temperature" ], 0 ) ) ) + scaleUnits,
        #"Humidity: " + str( weatherResponse[ "currently" ][ "humidity" ] ) + "%",
        #"Wind:        " + str( int( round( weatherResponse[ "currently" ][ "windSpeed" ] * windFactor ) ) ) + windUnits + " " + get_cardinal_direction( weatherResponse[ "wind" ][ "deg" ] ),
        #"Clouds:     " + str( weatherResponse[ "currently" ][ "cloudCover" ] ) + "%",
        #"Sun:           " + time.strftime("%H:%M", time.localtime( weatherResponse[ "sys" ][ "sunrise" ] ) ) + " am, " + time.strftime("%I:%M", time.localtime( weatherResponse[ "sys" ][ "sunset" ] ) ) + " pm") )
    #print("details label text {}".format(weatherDetailsLabel.txt)

    #today    = weatherResponse[ "list" ][ 0 ]
    #tomo     = weatherResponse[ "list" ][ 8 ]
    #print("got today forecast: {}".format(today))
    #print("got tomorrow forecast: {}".format(tomo))

except Exception as e:
    print("displayCurrentWeather err: {}".format(e))




