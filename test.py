#!/usr/bin/python3

import time
import cherrypy

class HellowWorld(object):
    def index(self):
        return "Howedy"
    index.exposed = True

##############################################################################
#                                                                            #
#       Main                                                                 #
#                                                                            #
##############################################################################

def main():
        counter = 0
        cherrypy.quickstart(HellowWorld())
        while (True):
            counter += 1
            print("in loop {}".format(counter))
            time.sleep(5)
        print ("this is the end?")
        raise


if __name__ == '__main__':
        try:
                main()
        except OSError as er:
            print("err: {0}".format(er))
        except Exception as err:
            print("error: {0}".format(err))
        finally:
            print("caught an err")


