#!/usr/bin/python3
#  use spidev to read atod counts from the mcp3208, converts to volts, convert
#  to resistance, then use steiholtz equation to convert to temperature

import spidev
import time
import math
from time import strftime
import pywapi
import string

VREF = 5.0
MAXNUM = 4096
PLACES = 4
KTOC = 273.15
RCONST = 24900.0

# equation and coefficients from
#  www.apogeeinstruments.com/content/ST-100-110-200-300-manual.pdf
Acoef = 1.128241E-3
Bcoef = 2.341077E-4
Ccoef = 8.775468E-8

# calculate volts from count
def getVolts(data):
    return round((data * VREF)/ MAXNUM, PLACES)

# calculate the ohms of the thermististor from measured voltage
def getOhms(voltsIn):
    return (((VREF/voltsIn) - 1.0) * RCONST)

# read SPI data from MCP3208 chip, 8 possible adc's (0 thru 7)
def readadc(adcnum):
    if adcnum > 7 or adcnum < 0:
        return -1
    spi = spidev.SpiDev()
    spi.open(0, 0)
    spi.max_speed_hz = 200000

    r = spi.xfer2([(6 + ((adcnum&4) >> 2)), ((adcnum&3) << 6), 0])
    adcout = ((r[1] & 15) << 8) + r[2]
    return adcout

#thermistor reading function
def temp_get(adc, scale_units):
    value = readadc(adc)
    volts = getVolts(value)
    ohms = getOhms(volts)
    lnohm = math.log(ohms) # ln(ohms)

    # Steinhart Hart Equation: T = 1/(A + B[ln(ohm)] + C[ln(ohm)]^3)
    t1 = (Bcoef * lnohm) # B[ln(ohm)]
    t2 = Ccoef * math.pow(lnohm,3) # C[ln(ohm)]^3
    temp = 1/(Acoef + t1 + t2)
    tempc = temp - KTOC
    tempf = 32.0 + ((9.0 * tempc) / 5.0)

    #print out info

    if (scale_units == 'metric'):
        return tempc
    else:
        return tempf

########### main equivalent
spi = spidev.SpiDev()
spi.open(0, 0)
spi.max_speed_hz = 200000
temp_get(0, "metric")

#log = open('logtest5.txt', 'w') #open a text file for logging
#print(log) #print what the log file is
#log.write('Time,TemperatureC\n') #write to log
#log.write('%s,%f\n' % (strftime("%H:%M"), temp_get(0, 'metric'))) #write to log
#log.write('%s,%f\n' % (strftime("%H:%M"), temp_get(0, 'else'))) #write to log

